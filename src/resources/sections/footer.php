<?PHP
use DynamicalWeb\HTML;
?>
<footer class="footer">
    <div class="container-fluid clearfix">
        <span class="d-block text-center text-sm-left d-sm-inline-block">Copyright © 2017-<?PHP HTML::print(date('Y')); ?>
            <a href="https://www.intellivoid.info/" target="_blank">Intellivoid</a>. All rights reserved.
        </span>
    </div>
</footer>